import { Component, OnInit } from '@angular/core';
import { ReqresService } from 'src/app/reqres.service';
// import { UserItemComponent } from '../user-item/user-item.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  public users: any[];
  constructor(private reqres: ReqresService) { }

  ngOnInit(): void {
    this.reqres.list()
      .subscribe((response: any)=>{
        // response.data.map(user=>{
        //   console.log(user);
        // })
        this.users = response.data;
      });
  }

}
