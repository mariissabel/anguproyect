import { Component, OnInit } from '@angular/core';
import { ReqresService } from 'src/app/reqres.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-regist-form',
  templateUrl: './regist-form.component.html',
  styleUrls: ['./regist-form.component.css']
})
export class RegistFormComponent implements OnInit {

  public email: string = '';
  public password: string = '';
  public password2: string = '';

  constructor(private reqres: ReqresService, private router: Router,) { }

  ngOnInit(): void {
  }
  onSave(evt: Event): void {
    evt.preventDefault();
    this.reqres.register({email: this.email, password: this.password, password2: this.password2 })
      .subscribe((response: any)=>{
        console.log(response);
        if(response.token) {
          this.router.navigate(['/users']);
        }
      });
    // console.log('Formulario completado. Email: '+ this.email + '\nPassword: ' + this.password);
  }
}

