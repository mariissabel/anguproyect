import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { ReqresService } from 'src/app/reqres.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-log-form',
  templateUrl: './log-form.component.html',
  styleUrls: ['./log-form.component.css']
})
export class LogFormComponent implements OnInit {
  public email: string = '';
  public password: string = '';

  constructor(private reqres: ReqresService, private router: Router,) { }

  ngOnInit(): void {
  }
  onSave(evt: Event): void {
    evt.preventDefault();
    this.reqres.login({email: this.email, password: this.password})
      .subscribe((response: any)=>{
        console.log(response);
        if(response.token) {
          this.router.navigate(['/users']);
        }
      });
    // console.log('Formulario completado. Email: '+ this.email + '\nPassword: ' + this.password);
  }
}
