import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.css']
})
export class UserItemComponent implements OnInit, OnChanges {
  @Input() user: any;
  constructor() { }
  ngOnChanges(): void {
    console.log(this.user);
  }

  ngOnInit(): void {
  }

}
