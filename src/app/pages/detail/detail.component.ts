import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  id: Number;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() :void{
    this.route.paramMap.subscribe(params => {
      this.id = Number(params.get('id'));
    });
  }

}
