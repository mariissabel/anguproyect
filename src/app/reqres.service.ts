import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReqresService {

  constructor(private http: HttpClient) {}
  login(user) {
      return this.http.post('https://reqres.in/api/login',user);
  }
  register(user) {
      return this.http.post('https://reqres.in/api/register',user);
  }
  list() {
      return this.http.get('https://reqres.in/api/users?per_page=120');
  }
  create(user) {
      return this.http.post('https://reqres.in/api/users',user);
  }
  update(id,user) {
      return this.http.put('https://reqres.in/api/users/' + id ,user);
  }
}
